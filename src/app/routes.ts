import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';

export const routes  = [
  { path: '', component: LoginComponent, pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
];
