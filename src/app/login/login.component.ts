import { Component, OnInit } from '@angular/core';

import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public email : string;
  public password : string;
  public msg : string;

  constructor(private authS : AuthService, private route : Router) { }

  ngOnInit() {}

  login(){
    if(!this.email || !this.password){
      this.msg = "Email and password are required";
      return
    }
    this.authS.signIn({email: this.email,password: this.password})
    .subscribe((data)=>{
      this.route.navigate(['/home'])
    },(err)=>{
      this.msg = 'An error occurred'
    })
  }

}
