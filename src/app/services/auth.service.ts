import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { URL } from './config';

@Injectable()
export class AuthService{

  constructor(private http : HttpClient){}


  signIn(credentials){
    return this.http.post(URL+"/login",credentials)
  }
}
